
using UnityEngine;

public class AmbientMusicTrigger : MonoBehaviour {
    public AudioClip clip;
    public string targetController = "AmbientMusicController";

    private void Start() {
        Play();
        Destroy(gameObject);
    }

    private void Play() {
        if (clip == null) {
            Debug.LogWarning("AudioClip not assigned for " + gameObject.name);
            return;
        }

        GameObject mcObj = GameObject.Find(targetController);
        if (mcObj == null) {
            Debug.LogWarning("No game object named " + targetController + " found");
            return;
        }

        MusicController mc = mcObj.GetComponent<MusicController>();

        if(mc == null) {
            Debug.LogWarning("MusicController not assigned for " + mcObj.name);
            return;
        }

        mc.QueueMusic(clip);
    }
}
