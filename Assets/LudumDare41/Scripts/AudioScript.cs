﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioScript : MonoBehaviour
{

    public AudioMixer masterMixer;

    public void SetSound(float SoundLevel)
    {
        masterMixer.SetFloat("musicVol", SoundLevel);
    }
}