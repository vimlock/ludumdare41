using UnityEngine;

public class BackgroundMusicTrigger : MonoBehaviour {
    public MusicPlaylist playlist;
    public string targetController = "BackgroundMusicController";

    private MusicController musicController;
    private int currentIndex = 0;

    private void Start() {
        if (playlist == null) {
            Debug.LogWarning("No MusicPlaylist given for " + gameObject.name);
            return;
        }

        GameObject mcObj = GameObject.Find(targetController);
        if (mcObj == null) {
            Debug.LogWarning("No game object named " + targetController + " found");
            return;
        }

        musicController = mcObj.GetComponent<MusicController>();
        if(musicController == null) {
            Debug.LogWarning("MusicController not assigned for " + mcObj.name);
            return;
        }


        ListenForEvents();
        PlayNext();
    }

    private void ListenForEvents() {

        musicController.onFinished.AddListener(PlayNext);
    }

    private void PlayNext() {

        currentIndex = Random.Range(0, playlist.NumTracks());
        AudioClip track = playlist.GetTrack(currentIndex);
        if (track == null) {
            Debug.LogWarning("Playlist track " + currentIndex + " not found");
            return;
        }

        musicController.QueueMusic(track);
    }
}
