﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform target;
    public float speed = 5.0f;
    public float smooth = 0.5f;

    public Camera cam;
    public float zoomDistance = 0.0f;
    public float zoomMaxDistance = 10.0f;
    public float zoomSensitivity = 2.0f;
    public float zoomSpeed = 1.0f;

    public float turnSpeed = 1.0f;
    private Quaternion targetRot;

    private Vector3 velocity;

    private float zoomBase;
    private float zoomCurrentDistance = 0.0f;

	void Start () {
        targetRot = transform.rotation;

        if (cam == null) {
            Debug.LogWarning("Camera missing from " + gameObject.name);
        }

        if (cam) {
            zoomBase = cam.orthographicSize;
            zoomMaxDistance = Mathf.Min(zoomMaxDistance, cam.orthographicSize - 1.0f);
        }

        zoomDistance = zoomMaxDistance * 0.5f;
	}

    void Update() {
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A)) {
            targetRot *= Quaternion.LookRotation((Vector3.left + Vector3.forward).normalized);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D)) {
            targetRot *= Quaternion.LookRotation((Vector3.right + Vector3.forward).normalized);
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W)) {
            zoomDistance += zoomSensitivity;
        }

        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S)) {
            zoomDistance -= zoomSensitivity;
        }

        zoomDistance = Mathf.Clamp(zoomDistance, 0, zoomMaxDistance);
    }


	void LateUpdate () {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRot, turnSpeed * Time.deltaTime);

        if (cam) {
            zoomCurrentDistance = Mathf.Lerp(zoomCurrentDistance, zoomDistance, zoomSpeed * Time.deltaTime);
            cam.orthographicSize = zoomBase - zoomCurrentDistance;
        }

        if (target == null) {
            return;
        }

        if (Vector3.Distance(transform.position, target.position) > 20.0f) {
            Snap();
        }
        else {
            transform.position = Vector3.SmoothDamp(
                    transform.position, target.position, ref velocity, smooth, speed
            );
        }
	}

    public void Snap() {
        if (target != null) {
            transform.position = target.position;
        }
    }
}
