using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct IntVector2 {
    public int x;
    public int y;

    public IntVector2(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public override string ToString() {
        return x + ", " + y;
    }
}

public class Chunk {

    public IntVector2 index;
    public GameObject root;
    public float lifetime; 
    public bool dirty;
}
