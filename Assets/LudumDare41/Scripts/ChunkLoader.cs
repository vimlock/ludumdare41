﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SurfaceResponse {

    [System.Serializable]
    public struct Surface {
        public int id;
        public string pixels;
    }

    public List<Surface> surfaces;
}

public class ChunkLoader : MonoBehaviour {

    public int seed = 0;
    public int chunkCount = 200;
    public float chunkSize = 50.0f;
    public Transform viewPosition;

    public float chunkCollectInterval = 1.0f;
    public float chunkMaxCacheDuration = 5.0f;

    public int tilesInChunk = 5;

    public GenParams genParams;
    public Tileset tileset;

    public string serverUrl = "http://localhost"; 
    public bool allowMultiplayer = true;

    public int maxSurfacesPerUpdate = 100;

    private float chunkCollectTimer = 0.0f;

    private Chunk[,] chunks;
    private List<Chunk> loadedChunks;

    private List<PaintSurface> surfacesToLoad;

    private bool loadedChunkThisFrame = false;

    private float surfacesPerSecond = 0.0f;
    private float chunkDirtyCheckTimer = 0.0f;

    private void Awake() {
        chunks = new Chunk[chunkCount, chunkCount];
        loadedChunks = new List<Chunk>();
        surfacesToLoad = new List<PaintSurface>();
    }

	private void Start () {
        if (!genParams) {
            Debug.LogError("Generation params not assigned for chunk loader");
        }

        if (!tileset) {
            Debug.LogError("No tileset given for chunk loader");
        }
        else if (tileset.tiles.Count == 0) {
            Debug.LogError("Empty tileset given for chunk loader");
        }

        if (string.IsNullOrEmpty(serverUrl)) {
            allowMultiplayer = false;
        }

        if (allowMultiplayer) {
            StartCoroutine(SurfaceLoadThread());
        }
	}
	
	private void Update () {

        loadedChunkThisFrame = false;

        if (chunkCollectTimer > 0.0f)
            chunkCollectTimer -= Time.deltaTime;

        if (chunkCollectTimer <= 0.0f) {
            CollectOldChunks(chunkCollectInterval);
            chunkCollectTimer = chunkCollectInterval;
        }

        if (viewPosition == null) {
            return;
        }

        LoadChunksNear(viewPosition.position.x, viewPosition.position.z);

        if (chunkDirtyCheckTimer > 0.0f) {
            chunkDirtyCheckTimer -= Time.deltaTime;
        }

        if (chunkDirtyCheckTimer <= 0.0f) {
            foreach (Chunk chunk in loadedChunks) {
                if (chunk != null && chunk.dirty) {
                    UpdateRemoteChunk(chunk);
                }
            }
        }
	}

    private void OnGUI() {
        
        if (!GameConfig.showDebugText) {
            return;
        }

        if (surfacesToLoad.Count > 0) {
            GUI.Label(new Rect(10, 10, 200, 20),
                string.Format("Loading surfaces {0,4}", surfacesToLoad.Count));

            GUI.Label(new Rect(10, 30, 200, 20),
                    string.Format("Surfaces per second: {0, 4}", surfacesPerSecond.ToString("0.0")));
        }


        GUI.Label(new Rect(10, 50, 200, 20),
                string.Format("Chunks loaded {0, 3}", loadedChunks.Count));

    }

    private IEnumerator SurfaceLoadThread() {

        Debug.Log("PaintSurface load coroutine started");

        List<PaintSurface> s = new List<PaintSurface>();

        while (true) {

            s.Clear();

            int i = 0;
            while (i < maxSurfacesPerUpdate && surfacesToLoad.Count != 0) {

                s.Add(surfacesToLoad[0]);

                // Swap and pop
                surfacesToLoad[0] = surfacesToLoad[surfacesToLoad.Count - 1];
                surfacesToLoad.RemoveAt(surfacesToLoad.Count - 1);

                i++;
            }

            float startTime = Time.time;
            int surfacesProcessed = 0;

            if (s.Count > 0) {

                string surfaces = string.Join(",", s.Select(x => x.id.ToString()).ToArray());
                string url = serverUrl + "/world/" + seed + "/surface/" + surfaces;

                // Debug.Log("GET " + url);

                WWW www = new WWW(url);
                yield return www;

                if (!string.IsNullOrEmpty(www.error)) {
                    Debug.LogWarning("GET failed " + www.error);
                    continue;
                }

                // Debug.Log(www.text);
                
                SurfaceResponse response = JsonUtility.FromJson<SurfaceResponse>(www.text);

                if (response == null) {
                    Debug.LogWarning("Failed to parse server response");
                    continue;
                }

                if (response.surfaces == null) {
                    Debug.LogWarning("Corrupted response from server");
                    continue;
                }

                foreach (var surf in response.surfaces) {
                    int id = surf.id;
                    byte[] data = System.Convert.FromBase64String(surf.pixels);

                    if (data.Length != 32 * 32 * 4) {
                        Debug.LogWarning("Corrupted surface image on surface " + id);
                        continue;
                    }

                    PaintSurface ps = s.Find(x => x.id == id);
                    if (ps == null) {
                        Debug.LogWarning("Received unknown paint surface in response");
                        continue;
                    }

                    ps.LoadFromPixels(data);
                }

                surfacesProcessed += s.Count;
            }

            float endTime = Time.time;
            float delta = endTime - startTime;

            delta = Mathf.Max(delta, 0.0000001f);
            float performance = surfacesProcessed / delta;

            surfacesPerSecond = Mathf.Lerp(performance, surfacesPerSecond, 0.9f);

            yield return null;
        }
    }

    public void MarkChunkDirty(IntVector2 index) {
        if (!IsChunkInRange(index)) {
            return;
        }

        if (chunks[index.x, index.y] != null) {
            chunks[index.x, index.y].dirty = true;
        }
        else {
            Debug.LogWarning("Trying to mark unloaded chunk as dirty");
            return;
        }

        chunkDirtyCheckTimer = 1.0f;
    }

    private bool IsChunkInRange(IntVector2 index) {
        if (index.x < 0 || index.y < 0) {
            Debug.LogWarning("Trying to access chunk out of range (" + index + ")");
            return false;
        }

        if (index.x >= chunkCount || index.y >= chunkCount) {
            Debug.LogWarning("Trying to access chunk out of range (" + index + ")");
            return false;
        }

        return true;
    }

    private void LoadChunksNear(float x, float y) {
        float halfChunk = chunkSize * 0.5f;

        Vector3 origin = new Vector3(
            transform.position.x - chunkCount * halfChunk,
            transform.position.y,
            transform.position.z - chunkCount * halfChunk
        );

        IntVector2 index = new IntVector2(
            (int)Mathf.Floor((x - origin.x) / chunkSize),
            (int)Mathf.Floor((y - origin.z) / chunkSize)
        );

        LoadChunkRegion(index.x - 1, index.y - 1, 3, 3);
    }

    private void LoadChunkRegion(int x, int y, int width, int height) {
        for (int i = 0; i < width; ++i) {
            for (int k = 0; k < height; ++k) {

                int cx = x + i;
                int cy = y + k;

                if (cx < 0 || cy < 0 || cx >= chunkCount || cy >= chunkCount) {
                    continue;
                }

                LoadChunk(cx, cy);
            }
        }
    }

    private void LoadChunk(int x, int y) {
        LoadChunk(new IntVector2(x, y));
    }

    private void LoadChunk(IntVector2 index) {
        if (loadedChunkThisFrame)
            return;

        if (!IsChunkInRange(index)) {
            return;
        }

        // Chunk already loaded?
        if (chunks[index.x, index.y] != null) {

            // Refresh the chunk lifetime
            chunks[index.x, index.y].lifetime = 0.0f;
            return;
        }

        Chunk chunk = new Chunk();
        chunks[index.x, index.y] = chunk;
        loadedChunks.Add(chunk);

        chunk.index = index;
        chunk.dirty = false;
        chunk.root = new GameObject("Chunk (" + index + ")");
        chunk.root.transform.SetParent(this.transform, false);
        chunk.root.transform.localPosition = new Vector3(
            index.x * chunkSize - chunkCount * chunkSize * 0.5f,
            0.0f,
            index.y * chunkSize - chunkCount * chunkSize * 0.5f
        );

        GenerateChunk(chunk);

        loadedChunkThisFrame = true;

        // Debug.Log("Loaded chunk " + index);
    }

    private void UnloadChunk(int x, int y) {
        UnloadChunk(new IntVector2(x, y));
    }

    private void UnloadChunk(IntVector2 index) {
        if (!IsChunkInRange(index)) {
            return;
        }

        // Chunk already unloaded?
        if (chunks[index.x, index.y] == null) {
            return;
        }

        Chunk chunk = chunks[index.x, index.y];
        chunks[index.x, index.y] = null;

        if (allowMultiplayer) {
            UpdateRemoteChunk(chunk);
        }

        Destroy(chunk.root);

        int loadedIndex = loadedChunks.FindIndex(x => x == chunk);

        if (loadedIndex >= 0) {
            loadedChunks[loadedIndex] = loadedChunks[loadedChunks.Count - 1];
            loadedChunks.RemoveAt(loadedChunks.Count - 1);
        }
        else {
            Debug.LogWarning("Chunk not found in loadedChunks");
        }

        // Debug.Log("Unloaded chunk " + index);
    }

    private void UpdateRemoteChunk(Chunk chunk) {
        PaintSurface[] pss = chunk.root.GetComponentsInChildren<PaintSurface>();

        foreach (PaintSurface ps in pss) {

            if (!ps.dirty) {
                continue;
            }

            if (ps.id < 0) {
                Debug.LogWarning("id not assigned for paint surface");
                continue;
            }

            ps.dirty = false;

            StartCoroutine(UploadSurface(ps));
        }
    }

    private IEnumerator UploadSurface(PaintSurface ps){

        string url = serverUrl + "/world/" + seed + "/surface/" + ps.id;

        Debug.Log("POST " + url);

        WWWForm postData = new WWWForm();
        postData.AddField("pixels", EncodePixels(ps.surf.GetPixels()));

        using (WWW www = new WWW(url, postData)) {
            yield return www;
        }
    }

    private string EncodePixels(Color[] pixels) {
        byte [] bytes = new byte[32 * 32 * 4];

        for (int i = 0; i < pixels.Length; ++i) {
            Color pixel = pixels[i];

            bytes[i * 4 + 0] = (byte)(pixel.r * 255.0f);
            bytes[i * 4 + 1] = (byte)(pixel.g * 255.0f);
            bytes[i * 4 + 2] = (byte)(pixel.b * 255.0f);
            bytes[i * 4 + 3] = (byte)(pixel.a * 255.0f);
        }

        return System.Convert.ToBase64String(bytes);
    }

    private void ReloadChunk(int x, int y) {
        ReloadChunk(new IntVector2(x, y));
    }

    private void ReloadChunk(IntVector2 index) {
        UnloadChunk(index);
        LoadChunk(index);
    }

    private void GenerateChunk(Chunk chunk) {

        float tileInterval = chunkSize / tilesInChunk;
        Vector3 chunkPos = chunk.root.transform.localPosition;

        // Freakin perlin noise
        Random.InitState(seed);

        const int maxSurfacesInChunk = 1024;

        int chunkId = chunk.index.x * chunkCount + chunk.index.y;
        int surfaceIdBase = chunkId * maxSurfacesInChunk;
        int nextSurfaceId = 0;

        // Deterministic seed for each chunk
        int chunkSeed = chunkId + seed;
        LCRandom r = new LCRandom(chunkSeed);

        LCRandom foodRand = new LCRandom(Random.Range(0, 1 << 16));

        for (int x = 0; x < tilesInChunk; x++) {
            for (int y = 0; y < tilesInChunk; y++) {

                float tileX = chunkPos.x + x * tileInterval;
                float tileY = chunkPos.z + y * tileInterval;

                float rx = (chunk.index.x + x) / (float)chunkCount * tilesInChunk;
                float ry = (chunk.index.y + y) / (float)chunkCount * tilesInChunk;

                float chance = r.NextFloat();

                float rand = Mathf.PerlinNoise(rx * genParams.buildingsNoise, ry * genParams.buildingsNoise);

                // Debug.Log(rx + " " + ry + " " + rand + " " + chance);

                if (rand < genParams.buildingsTreshold) {
                    continue;
                }

                if (chance >= genParams.buildingsDensity) {
                    continue;
                }

                int tileIndex = r.NextInt() % tileset.NumTiles();
                int tileRot = (r.NextInt() % 4);

                // Debug.Log("Spawning tile " + tileIndex);

                GameObject tilePrefab = tileset.GetTile(tileIndex);
                if (!tilePrefab) {
                    continue;
                }

                GameObject tile = Instantiate(tilePrefab);

                tile.transform.RotateAround(
                    tile.transform.position + new Vector3(-tileInterval * 0.5f, 0, -tileInterval * 0.5f),
                    Vector3.up,
                    90.0f * tileRot
                );

                tile.transform.localPosition += new Vector3(tileX, 0.0f, tileY);
                tile.transform.SetParent(chunk.root.transform, true);

                PaintSurface[] pss = chunk.root.GetComponentsInChildren<PaintSurface>();

                foreach (PaintSurface ps in pss) {
                    if (nextSurfaceId < maxSurfacesInChunk) {
                        ps.id = surfaceIdBase + nextSurfaceId;
                    }

                    ps.chunkLoader = this;
                    ps.chunkIndex = chunk.index;

                    nextSurfaceId++;
                    QueuePaintSurface(ps);
                }

                if (nextSurfaceId >= maxSurfacesInChunk) {
                    Debug.LogError("Max surface id exceeded: " + nextSurfaceId);
                }

                GenTileFoodItems(tile, foodRand);
            }
        }
    }

    private void QueuePaintSurface(PaintSurface ps) {
        if (ps.id < 0) {
            Debug.LogError("Bad surface id");
            return;
        }

        if (!allowMultiplayer) {
            return;
        }

        surfacesToLoad.Add(ps);
    }

    private void GenTileFoodItems(GameObject tileRoot, LCRandom rand) {

        if (genParams.foodTypes == null || genParams.foodTypes.Count == 0) {
            return;
        }

        List<GameObject> foodSpawners = new List<GameObject>();
        GetFoodSpawnersInChildren(tileRoot, ref foodSpawners);

        // Debug.Log("found " + foodSpawners.Count + " spawners");

        foreach (GameObject foodSpawnPoint in foodSpawners) {
            if (rand.NextFloat() >= genParams.foodDensity) {
                continue;
            }

            FoodType type = genParams.foodTypes[Random.Range(0, genParams.foodTypes.Count)];
            if (type == null || type.model == null) {
                // Debug.LogWarning("Bad food item " + type);
                continue;
            }

            GameObject foodItem = Instantiate(type.model);

            foodItem.transform.position = foodSpawnPoint.transform.position;
            foodItem.transform.rotation = foodSpawnPoint.transform.rotation * foodItem.transform.rotation;

            foodItem.transform.SetParent(tileRoot.transform, true);

            // Debug.Log("Food spawned");
        }
    }

    private void GetFoodSpawnersInChildren(GameObject obj, ref List<GameObject> spawners) {

        if (obj.tag == "FoodSpawn") {
            spawners.Add(obj);
        }

        foreach (Transform child in obj.transform) {
            GetFoodSpawnersInChildren(child.gameObject, ref spawners);
        }
    } 

    private void CollectOldChunks(float deltaTime) {
        
        int i = 0;
        while (i < loadedChunks.Count) {

            Chunk c = loadedChunks[i];
            c.lifetime += deltaTime;

            if (c.lifetime > chunkMaxCacheDuration) {
                UnloadChunk(c.index);

                return;
            }
            else {
                ++i;
            }
        }
    }

    public Vector3 GetSpawnPoint() {
        float halfChunk = chunkSize * 0.5f;

        Vector3 origin = new Vector3(
            transform.position.x - chunkCount * halfChunk,
            transform.position.y,
            transform.position.z - chunkCount * halfChunk
        );

        float x = chunkSize * Random.Range(chunkCount / 4, (chunkCount / 4) * 3) - origin.x;
        float z = chunkSize * Random.Range(chunkCount / 4, (chunkCount / 4) * 3) - origin.z;

        return new Vector3(x, 0, z);
    }
}
