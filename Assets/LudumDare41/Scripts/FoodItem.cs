using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodItem : Item {
    public float hungerReduction = 0.0f;

    private void Start() {
    }

    public override void OnPickup(PlayerCharacter player) {
        player.RemoveHunger(hungerReduction);
    }
}
