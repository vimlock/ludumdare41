using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FoodType {
    public GameObject model;
    public float hungerReduction = 10.0f;
}

[CreateAssetMenu(fileName = "GenParams", menuName = "LDJam/GenParams", order = 1)]
public class GenParams : ScriptableObject {

    [RangeAttribute(0.0f, 1.0f)]
    public float buildingsTreshold = 0.5f;

    [RangeAttribute(0.0f, 1.0f)]
    public float buildingsDensity = 0.5f;

    [RangeAttribute(0.1f, 5.0f)]
    public float buildingsNoise = 1.0f;

    [RangeAttribute(0.0f, 1.0f)]
    public float roadsDensity = 0.5f;

    [RangeAttribute(0.0f, 1.0f)]
    public float foodDensity = 0.5f;

    public List<FoodType> foodTypes;
}

