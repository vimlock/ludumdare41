
using UnityEngine;

public abstract class Item : MonoBehaviour {

    public string itemName = "unnamed";

    public virtual void OnPickup(PlayerCharacter player) {
    }
}
