
using UnityEngine;

public class ItemHighlight : MonoBehaviour {
    public float duration = 0.1f;
    public float lifetime = 0.0f;
    
    private Renderer rend;
    private Material origMat;
    private Material cloneMat;

    private void Start() {
        rend = GetComponent<MeshRenderer>();

        if (rend == null) {
            Debug.LogWarning("ItemHighlight added on object without MeshRenderer");
            return;
        }

        origMat = rend.material;

        if (origMat == null) {
            Debug.LogWarning("ItemHighlight added to MeshRenderer without material");
            return;
        }

        cloneMat = Instantiate(origMat) as Material;
        if (cloneMat == null) {
            Debug.LogWarning("Failed to clone Material");
            return;
        }

        Color col = cloneMat.GetColor("_OutlineColor");
        col.a = 0.5f;
        cloneMat.SetColor("_OutlineColor", col);

        rend.material = cloneMat;

    }

    private void OnDestroy() {
        if (rend != null && origMat) {
            rend.material = origMat;
        }
    }

    private void Update() {

        lifetime -= Time.deltaTime;

        if (lifetime <= 0.0f) {
            Destroy(this);
        }
    }

    public void RefreshLifetime() {
        lifetime = duration;
    }
}
