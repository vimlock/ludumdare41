using UnityEngine;

public class LCRandom {

    private int seed;

    public LCRandom(int seed) {
        this.seed = seed;
    }

    public int NextInt() {
        seed = (1103515245 * seed + 12345);

        return seed < 0 ? -seed : seed;
    }

    public float NextFloat() {
        int tmp = NextInt();
        int bignum = (1 << 16);

        float r = (tmp % bignum) / (float)bignum;

        Debug.Assert(r >= 0.0f && r <= 1.0f, "am a fucking idiot, cant make even a simple RNG without fucking up");

        return r;
    }
}
