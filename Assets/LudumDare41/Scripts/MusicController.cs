using UnityEngine;
using UnityEngine.Events;



public class MusicController : MonoBehaviour {

    public AudioSource audioSource;
    public UnityEvent onFinished;

    private float timeLeft = 0.0f;

    private void Awake() {
        DontDestroyOnLoad(gameObject);

        onFinished = new UnityEvent();

        if (audioSource == null) {
            audioSource = GetComponent<AudioSource>();
        }

        if (audioSource == null) {
            Debug.LogWarning("AudioSource missing from " + gameObject.name);
        }
    }

    private void Update() {
        if (!audioSource)
            return;

        if (audioSource.isPlaying) {
            if (timeLeft > 0.0f) {
                timeLeft -= Time.deltaTime;

                if (timeLeft <= 0.0f) {
                    onFinished.Invoke();
                }
            }
        }
    }

    public void QueueMusic(AudioClip clip) {
        if (clip == null) {
            Debug.LogWarning("Trying to play a missing audio clip!");
            return;
        }


        if (!audioSource) {
            Debug.LogWarning("No audio source to play from");
            return;
        }

        if (clip == audioSource.clip) {
            return;
        }

        audioSource.clip = clip;
        audioSource.Play();
    }
}
