using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MusicPlaylist", menuName = "LDJam/MusicPlaylist", order = 1)]
public class MusicPlaylist : ScriptableObject {

    public List<AudioClip> tracks;

    public int NumTracks() {
        return tracks == null ? 0 : tracks.Count;
    }

    public AudioClip GetTrack(int num) {
        if (tracks == null)
            return null;

        if (num < 0 || num >= tracks.Count) {
            return null;
        }

        return tracks[num];
    }

}
