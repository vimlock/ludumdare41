
using UnityEngine;

[System.Serializable]
public class Brush {

    public float jitter = 0.0f;
    public float strokeInterval = 0.0f;
    public int size = 1;
    public Color color = Color.red;
}

public class PaintSurface : MonoBehaviour {

    public Renderer rend;
    public Texture2D surf;

    public int id = -1;
    public bool dirty = false;

    public ChunkLoader chunkLoader;
    public IntVector2 chunkIndex;
    
    private void Start() {
        rend = GetComponent<Renderer>();

        Texture2D source = rend.material.mainTexture as Texture2D;

        surf = new Texture2D(32, 32, TextureFormat.RGBA32, false);
        surf.filterMode = FilterMode.Point;
        surf.wrapMode = TextureWrapMode.Clamp;

        if (source) {
            for (int x = 0; x < 32; x++) {
                for (int y = 0; y < 32; y++) {
                    surf.SetPixel(x, y, source.GetPixel(x, y));
                }
            }
        }
        else {
            for (int x = 0; x < 32; x++) {
                for (int y = 0; y < 32; y++) {
                    surf.SetPixel(x, y, new Color(0, 0, 0, 0));
                }
            }
        }

        surf.Apply();

        rend.material.mainTexture = surf;
    }

    private void OnDestroy() {

        if (surf) {
            // Debug.Log("Destroyed paint surface");
            Destroy(surf);
        }
    }

    public void LoadFromPixels(byte[] pixels) {
        if (surf == null) {
            Debug.Log("Can't load surface without texture!");
            return;
        }

        surf.LoadRawTextureData(pixels);
        surf.Apply();
    }

    public void Stroke(Brush brush, Vector2 uv) {

        dirty = true;

        if (chunkLoader) {
            chunkLoader.MarkChunkDirty(chunkIndex);
        }


        Vector2 pixel = new Vector2(uv.x * surf.width, uv.y * surf.height);

        pixel.x += Random.value * brush.jitter;
        pixel.y += Random.value * brush.jitter;

        int x = (int)pixel.x;
        int y = (int)pixel.y;

        if (brush.size <= 1) {
            surf.SetPixel(x, y, brush.color);
        }
        else if (brush.size <= 2) {
            surf.SetPixel(x, y, brush.color);
            surf.SetPixel(x + 1, y, brush.color);
            surf.SetPixel(x + 1, y + 1, brush.color);
            surf.SetPixel(x, y + 1, brush.color);
        }
        else if (brush.size <= 3) {
            surf.SetPixel(x, y, brush.color);

            surf.SetPixel(x + 1, y, brush.color);
            surf.SetPixel(x - 1, y, brush.color);

            surf.SetPixel(x, y + 1, brush.color);
            surf.SetPixel(x, y - 1, brush.color);
        }

        surf.Apply();
    }
}
