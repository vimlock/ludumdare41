﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

public class PlayerCharacter : MonoBehaviour {

    public bool isAlive = true;

    public float hunger = 0.0f;
    public float maxHunger = 100.0f;
    public float hungerGrowth = 1.0f;

    public float moveSpeed = 1.0f;
    public float pickRange = 1.0f;

    public float paintDistance = 4.0f;

    public Brush brush;

    public ChunkLoader chunkloader;
    public CameraController camController;

    public NavMeshAgent navAgent;
    public Animator anim;

    public MusicPlaylist footstepAudio;
    public float footstepInterval = 1.0f;

    public Item pickupTarget;

    private int nextFootstepAudio = 0;
    private float footstepTimer = 0.0f;

    [HideInInspector]
    public AudioSource audioSource;

    public float animationWaitTimer = 0.0f;
    public float paintAnimTimer = 0.0f; 

    private float curMoveSpeed = 0.0f;

    public bool animForceLook = false;
    public Vector3 animLookTarget;

    public GameObject sprayCan;
    public AudioClip spraySound;
    public float pitchOrigin = 0.0f;
    public float pitchRandom = 0.25f;

	// Use this for initialization
	void Start () {
        if (navAgent == null)
            navAgent = GetComponent<NavMeshAgent>();

        if (navAgent != null) {
            navAgent.stoppingDistance = 1.0f;
        }
        else {
            Debug.LogWarning("NavMeshAgent missing from " + gameObject.name);
        }

        if (anim == null) {
            Debug.LogWarning("AnimationController not assigned for " + gameObject.name);
        }

        if (footstepAudio == null) {
            Debug.LogWarning("Foostep audio missing");
        }
        else if (footstepAudio.NumTracks() == 0) {
            Debug.LogWarning("Empty footstep audio playlist");
        }

        if (audioSource == null) {
            audioSource = GetComponent<AudioSource>();
        }

        if (audioSource == null) {
            Debug.LogWarning("AudioSource missing from " + gameObject.name);
        }

        if (chunkloader != null)  {
            transform.position = chunkloader.GetSpawnPoint();

            if (camController != null) {
                camController.Snap();
            }
        }
	}
	
	// Update is called once per frame
	void Update () {

        AddHunger(hungerGrowth * Time.deltaTime);
        if (hunger >= maxHunger) {
            Kill();
        }

        if (!isAlive) {
            anim.SetBool("Alive", false);
            return;
        }

        if (paintAnimTimer > 0.0f) {

            sprayCan.SetActive(true);

            animationWaitTimer = 0.5f;
            animForceLook = true;

            paintAnimTimer -= Time.deltaTime;
        }
        else {
            sprayCan.SetActive(false);
        }

        if (navAgent != null) {
            // Smooth aout the nav mesh agents speed
            curMoveSpeed = Mathf.Lerp(curMoveSpeed, navAgent.velocity.magnitude, 0.25f);
        }

        if (animationWaitTimer > 0.0f) {
            animationWaitTimer -= Time.deltaTime;
        }
        else {
            animForceLook = false;
        }

        if (animForceLook) {
            Vector3 lookDir = animLookTarget - transform.position;
            lookDir.y = 0.0f;
            lookDir.Normalize();

            transform.rotation = Quaternion.Slerp(
                    transform.rotation, Quaternion.LookRotation(lookDir), Time.deltaTime * 1.0f
            );
        }

        if (curMoveSpeed > 0.5f) {
            footstepTimer += curMoveSpeed * Time.deltaTime;
        }

        if (audioSource != null) {
            if (paintAnimTimer > 0.0f && (audioSource.clip != spraySound || !audioSource.isPlaying)) {
                audioSource.clip = spraySound;
                audioSource.loop = true;
                audioSource.pitch = pitchOrigin + Random.value * pitchRandom;
                audioSource.Play();
            }
            else if (paintAnimTimer <= 0.0f && audioSource.clip == spraySound && audioSource.isPlaying) {
                audioSource.Stop();
            }
        }

        if (footstepTimer > footstepInterval) {
            footstepTimer -= footstepInterval;

            if (audioSource != null && footstepAudio != null) {
            
                AudioClip clip = footstepAudio.GetTrack(nextFootstepAudio);
                nextFootstepAudio = (nextFootstepAudio + 1) % footstepAudio.NumTracks();

                audioSource.loop = false;
                audioSource.pitch = pitchOrigin + Random.value * pitchRandom;
                audioSource.clip = clip;
                audioSource.Play();
            }
        }

        if (pickupTarget != null) {
            float dist = Vector3.Distance(
                pickupTarget.transform.position,
                transform.position
            );

            if (dist <= pickRange) {
                pickupTarget.OnPickup(this);
                Destroy(pickupTarget.gameObject);
                pickupTarget = null;

                animationWaitTimer = 0.5f;
                anim.SetTrigger("Pickup");
            }
        }

        if (anim != null) {
            anim.SetBool("Alive", isAlive);
            anim.SetFloat("MoveSpeed", curMoveSpeed);
            anim.SetBool("IsSpraying", paintAnimTimer > 0.0f);
        }
	}

    public void AddHunger(float amount) {
        hunger += amount;
        hunger = Mathf.Min(hunger, maxHunger);
    }

    public void RemoveHunger(float amount) {
        hunger -= amount;
        hunger = Mathf.Max(hunger, 0);
    }

    public void Kill() {
        if (!isAlive) {
            return;
        }

        StopActions();

        isAlive = false;

        Debug.Log(gameObject.name + " was killed");
    }

    public void MoveTo(Vector3 point) {
        if (IsInAnimation()) {
            return;
        }

        StopActions();

        if (navAgent == null) {
            return;
        }

        navAgent.isStopped = false;
        navAgent.SetDestination(point);
    }

    public bool IsInAnimation() {
        return !isAlive || animationWaitTimer > 0.0f;
    }

    public void StopActions() {
        pickupTarget = null;
        navAgent.isStopped = true;
    }

    public void PickUpItem(Item item) {
        if (IsInAnimation()) {
            return;
        }

        StopActions();

        pickupTarget = item;

        Debug.Log("Picking up item " + item.itemName);

        navAgent.isStopped = false;
        navAgent.SetDestination(item.transform.position);
    }
}
