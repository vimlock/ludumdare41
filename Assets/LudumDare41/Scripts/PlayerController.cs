using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using System.Collections.Generic;

public class PlayerController : MonoBehaviour {

    public PlayerCharacter player;

    public LayerMask obstructLayers = 0;
    public LayerMask traverseLayers = ~0;
    public LayerMask paintLayers = 0;
    public LayerMask itemLayers = 0;

    public List<Color> colorPalette;
    public int curColor = 0;

    public Image paintColorImage;
    public List<Image> paintSizeImages;

    public Image hungerMeter;

    public AudioSource audioSource;
    public AudioClip changeBrushColor;
    public AudioClip changeBrushSize;

    private void Awake() {
        if (colorPalette == null) {
            colorPalette = new List<Color>();
        }
    }

    private void Start() {
        if (paintLayers == 0) {
            Debug.LogWarning("No paint layers assigned for " + gameObject.name);
        }
        
        if (obstructLayers == 0) {
            Debug.LogWarning("No obstruct layers assigned for " + gameObject.name);
        }

        if (traverseLayers == 0) {
            Debug.LogWarning("No traverse layers assigned for " + gameObject.name);
        }

        if (itemLayers == 0) {
            Debug.LogWarning("No item layers assigned for " + gameObject.name);
        }

        if (colorPalette.Count == 0) {
            Debug.LogWarning("No colors in palette");
            colorPalette.Add(Color.white);
        }

        if (audioSource == null) {
            audioSource = GetComponent<AudioSource>();
        }

        if (audioSource == null) {
            Debug.LogWarning("AudioSource missing from " + gameObject.name);
        }

        ChangePaintColor(false);
        ChangePaintSize(3, false);
    }

    private void Update() {

        if (Input.GetKeyDown(KeyCode.Escape)) {
            SceneManager.LoadScene(0);
        }

        if (player == null || !player.isAlive) {
            hungerMeter.fillAmount = 1.0f;
            return;
        }

        hungerMeter.fillAmount = (player.hunger / player.maxHunger);

        if (player.brush != null) {
            if (Input.GetKeyDown(KeyCode.C)) {
                ChangePaintColor();
            }
        }

        if (Input.GetKeyDown(KeyCode.F1)) {
            GameConfig.showDebugText = !GameConfig.showDebugText;
        }

        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            ChangePaintSize(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2)) {
            ChangePaintSize(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3)) {
            ChangePaintSize(3);
        }

        CheckMouseHover(Input.mousePosition, Camera.main);
    }

    private void CheckMouseHover(Vector2 screenPos, Camera cam) {
        Ray ray = cam.ScreenPointToRay(screenPos);

        ray.origin += ray.direction * 15.0f;

        RaycastHit hit;

        int layers = obstructLayers | traverseLayers | paintLayers | itemLayers;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layers)) {

            GameObject hitObject = hit.collider.gameObject;
            int layer = 1 << hitObject.layer;

            if ((1 << hitObject.layer & paintLayers) != 0) {
                PaintSurface ps = hitObject.GetComponent<PaintSurface>();
                if (ps == null) {
                    Debug.LogWarning("Paint surface missing from " + hitObject.name);
                }

                if (Input.GetMouseButton(0)) {

                    if (Vector3.Distance(player.transform.position, hit.point) > player.paintDistance) {
                        player.MoveTo(hit.point);
                        return;
                    }


                    player.animLookTarget = hit.point;
                    player.animForceLook = true;
                    player.paintAnimTimer = 0.5f;

                    Paint(ps, hit.textureCoord);
                }
            }

            else if ((itemLayers & layer) != 0) {

                Item item = hitObject.GetComponent<Item>();
                if (item == null) {
                    Debug.LogWarning("Item missing from " + hitObject.name);
                    return;
                }

                ItemHighlight hl = hitObject.GetComponent<ItemHighlight>();
                if (hl != null) {
                    hl.RefreshLifetime();
                }
                else {
                    hl = hitObject.AddComponent<ItemHighlight>();
                }

                if (Input.GetMouseButtonDown(0)) {
                    player.PickUpItem(item);
                }
            }

            else if ((obstructLayers & layer) != 0) {
                // Hit a building or something
            }

            else if ((traverseLayers & layer) != 0) {
                if (Input.GetMouseButtonDown(0)) {
                    player.MoveTo(hit.point);
                }
            }
        }
    }

    private void ChangePaintColor(bool audio=true) {
        curColor = (curColor + 1) % colorPalette.Count;
        player.brush.color = colorPalette[curColor];

        if (paintColorImage) {
            paintColorImage.color = player.brush.color;
        }

        if (audio && audioSource != null && changeBrushColor != null) {
            audioSource.clip = changeBrushColor;
            audioSource.loop = false;
            audioSource.Play();
        }
    }

    private void ChangePaintSize(int newSize, bool audio=true) {
        player.brush.size = newSize;

        newSize = newSize - 1;

        newSize = Mathf.Min(newSize, paintSizeImages.Count);
        for (int i = 0; i < paintSizeImages.Count; ++i) {
            if (i == newSize) {
                paintSizeImages[i].color = Color.white;
            }
            else {
                paintSizeImages[i].color = Color.black;
            }
        }

        if (audio && audioSource != null && changeBrushSize != null) {
            audioSource.clip = changeBrushSize;
            audioSource.loop = false;
            audioSource.Play();
        }
    }

    private void Paint(PaintSurface surf, Vector2 uv) {

        if (surf == null) {
            Debug.LogWarning("No surface!");
            return;
        }

        if (player.brush == null) {
            Debug.LogWarning("No brush!");
            return;
        }

        player.StopActions();

        surf.Stroke(player.brush, uv);
    }
}
