
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Tileset", menuName = "LDJam/Tileset", order = 1)]
public class Tileset : ScriptableObject {
    public List<GameObject> tiles;

    public int NumTiles() {
        return tiles.Count;
    }

    public GameObject GetTile(int index) {
        if (index < 0 || index >= tiles.Count) {
            return null;
        }

        return tiles[index];
    }

    public GameObject GetRandom() {
        if (tiles.Count == 0) {
            return null;
        }

        return tiles[Random.Range(0, tiles.Count)];
    }
}

