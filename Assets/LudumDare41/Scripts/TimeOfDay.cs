using UnityEngine;
using System;

public class TimeOfDay : MonoBehaviour {

    public float cycleDuration = 60 * 0.5f;
    public float timeOffset = 0.0f;

    public AnimationCurve shadowStrength;
    public Gradient colorGradient;
    public bool syncTime = true;
    public bool rotate = true;

    private Light sun;
    private Vector3 sunRotation;
    private float baseShadowStrength;

    private void Start() {
        sun = GetComponent<Light>();
        if (sun == null) {
            Debug.LogWarning("Light missing from " + gameObject.name);
        }


        if (syncTime) {

            DateTime epoch = new System.DateTime(1970, 1, 1, 8, 0, 0, System.DateTimeKind.Utc);
            double seconds = (System.DateTime.UtcNow - epoch).TotalSeconds;

            timeOffset += (float)(seconds / cycleDuration);
            timeOffset %= 1.0f;
        }

        if (sun) {
            baseShadowStrength = sun.shadowStrength;
            sunRotation = sun.transform.localEulerAngles;
        }
    }

    private void Update() {
        if (sun == null)
            return;

        timeOffset = (timeOffset + Time.deltaTime * (1.0f / cycleDuration)) % 1.0f;

        sun.color = colorGradient.Evaluate(timeOffset);
        sun.shadowStrength = baseShadowStrength * shadowStrength.Evaluate(timeOffset);

        if (rotate) {
            sun.transform.localEulerAngles = new Vector3(sunRotation.x + 360.0f * timeOffset, sunRotation.y, sunRotation.z);
        }
    }
}
