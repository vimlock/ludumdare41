﻿Shader "LDJam/PaintSurface" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		[PerRenderData] _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _OpacityMask ("OpacityMask", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent"}

        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite On
        Offset -0.1, -1

		LOD 200
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows alpha:fade
		#pragma target 3.0

		sampler2D _MainTex;
        sampler2D _OpacityMask;

		struct Input {
			float2 uv_MainTex;
		};

		fixed4 _Color;

		UNITY_INSTANCING_CBUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_CBUFFER_END

		void surf (Input IN, inout SurfaceOutputStandard o) {

            fixed4 mask = tex2D (_OpacityMask, IN.uv_MainTex);
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Metallic = 0.0f;
			o.Smoothness = 1.0f;
			o.Alpha = mask.a * c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
