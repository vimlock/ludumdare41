
Shader "ToonPack/ToonDiffAlpha" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
        _MainTex("Texture", 2D) = "white" {}
        _ToonSize ("Size", Range(0, 1)) = 0.5
        _ToonSmooth ("Smooth", Range(0.001, 1)) = 0.1
        _ToonEmit ("Emit", Range(0, 2)) = 0.1
        _ToonSpecColor("Specular", Color) = (1, 1, 1, 1)
        _ToonSpecSize("Specular Size", Range(0.001, 75)) = 50
	}

	SubShader {
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }

        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        CGPROGRAM
        #pragma surface surf ToonForward addshadow fullforwardshadows alpha

        struct Input
        {
            float4 color : COLOR;
            float2 uv_MainTex: TEXCOORD0;
        };

        sampler2D _MainTex;

        float4 _Color;
        float _ToonSmooth;
        float _ToonEmit;
        float _ToonSize;
        float4 _ToonSpecColor;
        float _ToonSpecSize;

        half4 LightingToonForward(SurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
        {
			half NdotL = dot(s.Normal, lightDir);
            half diff = smoothstep(0.0, _ToonSmooth, NdotL + _ToonSmooth + (_ToonSize * 2.0f - 1.0f));

            half4 c;

            atten =
                0.2 * step(0.05, atten) +
                0.2 * step(0.25, atten) +
                0.2 * step(0.50, atten)
            ;

            half NdotH = max(0.0, dot(s.Normal, normalize(lightDir + viewDir)));
            half spec = NdotH;

            spec = pow(spec, _ToonSpecSize);
            spec = step(0.01, spec);

            half3 specCol = _LightColor0.rgb * spec * atten;
            half3 diffCol = _LightColor0.rgb * diff * atten;

            half3  col = (s.Albedo * diffCol + specCol * _ToonSpecColor.rgb);

            c.rgb = col;
            c.a = s.Alpha;

            return c;
        }

        void surf (Input IN, inout SurfaceOutput o)
        {
            fixed4 col = tex2D(_MainTex, IN.uv_MainTex) * _Color * IN.color;
            o.Albedo = col.rgb;
            o.Emission = o.Albedo * _ToonEmit;
            o.Alpha = col.a;
            o.Specular = 0;
        }

        ENDCG
    }
}
