
Shader "ToonPack/ToonDiffNormal" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
        _MainTex("Texture", 2D) = "white" {}
        _BumpMap("Normal Map", 2D) = "bumb" {}
        _ToonSize ("Size", Range(0, 1)) = 0.5
        _ToonSmooth ("Smooth", Range(0.001, 1)) = 0.1
        _ToonEmit ("Emit", Range(0, 2)) = 0.1
        _ToonSpecColor("Specular", Color) = (1, 1, 1, 1)
        _ToonSpecSize("Specular Size", Range(0.001, 75)) = 50
	}

	SubShader {
		Tags { "RenderType"="Opaque" }

        CGPROGRAM
        #pragma surface surf ToonForward addshadow fullforwardshadows 

        struct Input
        {
            float4 color : COLOR;
            float2 uv_MainTex: TEXCOORD0;
        };

        sampler2D _MainTex;
        sampler2D _BumpMap;

        float4 _Color;
        float _ToonSmooth;
        float _ToonEmit;
        float _ToonSize;
        float4 _ToonSpecColor;
        float _ToonSpecSize;

        half4 LightingToonForward(SurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
        {
			half NdotL = dot(s.Normal, lightDir);
            half diff = smoothstep(0.0, _ToonSmooth, NdotL + _ToonSmooth + (_ToonSize * 2.0f - 1.0f));

            half4 c;

            atten =
                0.2 * step(0.05, atten) +
                0.2 * step(0.25, atten) +
                0.2 * step(0.50, atten)
            ;

            half NdotH = max(0.0, dot(s.Normal, normalize(lightDir + viewDir)));
            half spec = NdotH;
            spec = pow(spec, _ToonSpecSize);
            spec = step(0.01, spec);

            c.rgb = s.Albedo * (_LightColor0.rgb * diff + _ToonSpecColor.rgb * _LightColor0.rgb * spec) * atten;
            c.a = s.Alpha;

            return c;
        }

        void surf (Input IN, inout SurfaceOutput o)
        {
            o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb * _Color.rgb * IN.color;
            o.Emission = o.Albedo * _ToonEmit;
            o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
            o.Alpha = 1.0;
            o.Specular = 0;
        }

        ENDCG
    }
}
